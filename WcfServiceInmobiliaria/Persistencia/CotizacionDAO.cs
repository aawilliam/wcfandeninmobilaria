﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WcfServiceInmobiliaria.Dominio;

namespace WcfServiceInmobiliaria.Persistencia
{
    public class CotizacionDAO
    {
        private string CadenaConexionDB = "Data Source=.;Initial Catalog=InmoviliariaAnden;Integrated Security=True";

        public Cotizacion Registrar(Cotizacion obj)
        {

            Cotizacion cotizacion = new Cotizacion();
            string sql = "INSERT INTO Solicitud(dni,nombres,apellidos,correo,telefono,estado) " +
                "VALUES(@dni,@nombres,@apellidos,@correo,@telefono,@estado)";
            using (SqlConnection conexion = new SqlConnection(CadenaConexionDB))
            {
                conexion.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conexion))
                {
                    //cmd.Parameters.Add(new SqlParameter("@id", obj.Id));
                    cmd.Parameters.Add(new SqlParameter("@dni", obj.Dni));
                    cmd.Parameters.Add(new SqlParameter("@nombres", obj.Nombres));
                    cmd.Parameters.Add(new SqlParameter("@apellidos", obj.Apellidos));
                    cmd.Parameters.Add(new SqlParameter("@correo", obj.Correo));
                    cmd.Parameters.Add(new SqlParameter("@telefono", obj.Telefono));
                    cmd.Parameters.Add(new SqlParameter("@estado", obj.Estado));
                    cmd.ExecuteNonQuery();
                }


                cotizacion = Obtener(obj.Dni);
                return cotizacion;

            }
        }

        public Cotizacion Obtener(string dni)
        {
            Cotizacion cotizacion = null;
            string sql = "select id,dni,nombres,apellidos,correo,telefono,estado from Solicitud where dni=@dni";
            using (SqlConnection conexion = new SqlConnection(CadenaConexionDB))
            {
                conexion.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conexion))
                {
                    cmd.Parameters.Add(new SqlParameter("@dni", dni));
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            cotizacion = new Cotizacion()
                            {
                                Id = Convert.ToInt32(reader["id"]),
                                Dni = Convert.ToString(reader["dni"]),
                                Nombres = Convert.ToString(reader["nombres"]),
                                Apellidos = Convert.ToString(reader["apellidos"]),
                                Correo = Convert.ToString(reader["correo"]),
                                Telefono = Convert.ToString(reader["telefono"]),
                                Estado = Convert.ToBoolean(reader["estado"])
                            };

                        }

                    }
                }

                return cotizacion;

            }
        }

        public List<Cotizacion> Listar() {
            List<Cotizacion> listarCotizacion = new List<Cotizacion>();
            Cotizacion cotizacion = null;
            string sql = "select id,dni,nombres,apellidos,correo,telefono,estado from Solicitud";
            using (SqlConnection conexion = new SqlConnection(CadenaConexionDB)) {
                conexion.Open();
                using (SqlCommand cmd = new SqlCommand(sql, conexion)) {

                    using (SqlDataReader objReader = cmd.ExecuteReader())
                    {

                        while (objReader.Read()) {

                            cotizacion = new Cotizacion()
                            {
                                Id = Convert.ToInt32(objReader["id"]),
                                Apellidos = Convert.ToString(objReader["apellidos"]),
                                Nombres = Convert.ToString(objReader["nombres"]),
                                Correo = Convert.ToString(objReader["correo"]),
                                Dni = Convert.ToString(objReader["dni"]),
                                Estado = Convert.ToBoolean(objReader["estado"]),
                                Telefono = Convert.ToString(objReader["telefono"])
                            };
                            listarCotizacion.Add(cotizacion);
                        }                    
                    }
                }
            }

            return listarCotizacion;

        }
    }
}