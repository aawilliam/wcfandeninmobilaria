﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WcfServiceInmobiliaria.Dominio;
using WcfServiceInmobiliaria.Errores;
using WcfServiceInmobiliaria.Persistencia;

namespace WcfServiceInmobiliaria
{
  
    public class ServiceInmobiliaria : IServiceCotizacion
    {
        CotizacionDAO cotizacionDao = new CotizacionDAO();

        public List<Cotizacion> Listar()
        {

            return cotizacionDao.Listar();
        }

        
        public Cotizacion RegistrarCotizacion(Cotizacion obj)
        {
            if (cotizacionDao.Obtener(obj.Dni)!=null) {
                throw new FaultException<RepetidorException>(new RepetidorException() { codigo = "404", descripcion = "Error al registrar" });
            }
            return cotizacionDao.Registrar(obj);
        }


    }
}
