﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WcfServiceInmobiliaria.Dominio
{
    [DataContract]
    public class Cotizacion
    {
       
        private int id;
       
        private string dni;
      
        private string nombres;
      
        private string apellidos;
 
        private string correo;

        private string telefono;
 
        private bool estado;

        
        public int Id { get; set; }
        [DataMember]
        public string Dni { get; set; }
        [DataMember]
        public string Nombres { get; set; }
        [DataMember]
        public string Apellidos { get; set; }
        [DataMember]
        public string Correo { get; set; }
        [DataMember]
        public string Telefono { get; set; }
        
        public bool Estado { get; set; }
    }
}